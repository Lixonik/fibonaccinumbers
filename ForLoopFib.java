public class ForLoopFib {
    public static void main(String[] args) {
        int s = 1, a = 0, b = 1;
        for (int i = 0; i < 11; i++) {
            System.out.print(s + " ");
            s = a + b;
            a = b;
            b = s;
        } // 1 1 2 3 5 8 13 21 34 55 89
    }
}
