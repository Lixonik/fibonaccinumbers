public class WhileLoopFib {
    public static void main(String[] args) {
        int s = 1, a = 0, b = 1;
        int i = 0;
        do {
            System.out.print(s + " ");
            s = a + b;
            a = b;
            b = s;
        } while (++i < 11); // 1 1 2 3 5 8 13 21 34 55 89
    }
}
